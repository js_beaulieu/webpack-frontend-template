const { getIfUtils } = require('webpack-config-utils')

module.exports = function (env) {
  return getIfUtils(env, ['development', 'staging', 'production'])
}
