const glob = require('glob')
const path = require('path')
const { removeEmpty } = require('webpack-config-utils')

module.exports = function (vendorsPath, jsEntry, stylesEntry) {
  const vendorFiles = glob.sync(path.join(__dirname, vendorsPath, '*.js'))

  return removeEmpty({
    vendors: vendorFiles.length ? vendorFiles : undefined,
    build: [
      path.join(__dirname, '../..', jsEntry),
      path.join(__dirname, '../..', stylesEntry)
    ]
  })
}
