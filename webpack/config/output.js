const path = require('path')
const { env } = require('../config')

module.exports = function (outputPath, outputDistPath) {
  const { ifProduction } = require('../helpers')(env)

  return {
    path: path.join(__dirname, '../..', outputPath),
    filename: ifProduction(
      `${outputDistPath}/[name].min.js`,
      `${outputDistPath}/[name].js`
    )
  }
}
