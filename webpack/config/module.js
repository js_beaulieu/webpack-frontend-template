const ExtractTextPlugin = require('extract-text-webpack-plugin')
const { env } = require('../config')

module.exports = function (config) {
  const { ifProduction } = require('../helpers')(env)

  return {
    loaders: [
      { test: /\.js?$/, loader: 'babel-loader', exclude: /node_modules/ },
      { test: /\.css$/, loader: 'style-loader!css-loader' },
      {
        test: /\.scss$/,
        loader: ExtractTextPlugin.extract({
          fallback: 'style-loader',
          use: ifProduction(
            ['css-loader', 'resolve-url-loader', 'sass-loader?sourceMap'],
            ['css-loader?modules', 'sass-loader']
          )
        })
      },
      {
        test: /\.(jpe?g|png|gif|svg)$/i,
        loader: 'url-loader',
        options: {
          limit: config.urlLoader.limit,
          name: '[name].[ext]',
          fallback: 'file-loader',
          publicPath: config.urlLoader.publicPath,
          outputPath: config.urlLoader.outputPath
        }
      }

    ]
  }
}
