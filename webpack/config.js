const dotenv = require('dotenv')

// Default configuration - don't edit these values, create a .env file.
const defaults = {
  // Source paths/files
  JS_ENTRY: 'src/js/custom/index.js',
  STYLES_ENTRY: 'src/scss/index.scss',
  VENDORS_PATH: 'src/js/vendors',
  ASSETS_PATH: 'src/assets',

  // Public paths
  OUTPUT_PATH: 'public',
  OUTPUT_DIST_PATH: 'dist', // relative to OUTPUT_PATH
  OUTPUT_IMG_PATH: '../img', // relative to OUTPUT_DIST_PATH

  URL_LOADER_MAX_SIZE: 1,
  NODE_ENV: 'development'
}
const config = Object.assign(defaults, dotenv.config().parsed || {})

module.exports = config
module.exports.env = config.NODE_ENV
