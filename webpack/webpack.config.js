
const path = require('path')
const entry = require('./config/entry')
const output = require('./config/output')
const modules = require('./config/module')
const plugins = require('./config/plugins')
const config = require('./config')

module.exports = {
  entry: entry(config.VENDORS_PATH, config.JS_ENTRY, config.STYLES_ENTRY),
  output: output(config.OUTPUT_PATH, config.OUTPUT_DIST_PATH),
  module: modules({
    urlLoader: {
      limit: config.URL_LOADER_MAX_SIZE,
      publicPath: config.OUTPUT_IMG_PATH,
      outputPath: path.join(config.OUTPUT_PATH, config.OUTPUT_IMG_PATH)
    }
  }),
  plugins: plugins(config.OUTPUT_DIST_PATH)
}
