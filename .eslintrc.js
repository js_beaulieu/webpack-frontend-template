module.exports = {
    extends: 'standard',
    parserOptions: {
        ecmaVersion: 7,
        sourceType: 'module'
    },
    env: { es6: true }
}
